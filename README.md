
# Bootstrap 5 Boilerplate

This is a repository for my custom Bootstrap 5 starter template

## Features

- [Flatly Bootstrap theme](https://bootswatch.com/flatly/)
- [modern-normalize.css](https://github.com/sindresorhus/modern-normalize)
- [Ionic Icons](https://ionic.io/ionicons)
- Light/dark mode toggle snippet
- PWA-ready

## Contacts

- [@cryingdaemon](https://t.me/cryingdaemon)
